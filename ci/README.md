# KWinFT's tooling CI tooling

## Linters
### Commit messages
Uses [commitlint][commitlint] with the [conventionalcommits preset][commitlint-preset] and
additional configuration in `ci/compliance/commitlint.config.js`.

[commitlint]: https://github.com/conventional-changelog/commitlint
[commitlint-preset]: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-conventionalcommits
