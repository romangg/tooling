# Common tooling for KWinFT projects

This repo contains scripts and other tools
to develop and maintain the KWinFT projects.

As of now the following tooling is provided:
* i18n: tools for internationalization
